<?php
class Vacancy
{
    public $id = 0;
    public $title = "";
    public $text_short = "";
    public $text = "";
    public $spec = "";
    public $spec_id = "";
    public $salary_from = 0;
    public $salary_to = 0;
    public $company_name = "";
    public $region = "";
    public $region_id = "";
    public $create_date = "";

    function __construct(
        int $id, string $title, string $text,
        string $salary_from, string $salary_to, string $company_name
    ) {
        $this->id = $id;
        $this->title = $title;
        $this->text = $text;
        $this->salary_from = $salary_from;
        $this->salary_to = $salary_to;
        $this->company_name = $company_name;
    }
}

class Company
{
    public $id = 0;
    public $name = "";
}

class Spec
{
    public $id = 0;
    public $name = "";
    public $desc = "";
}

?>