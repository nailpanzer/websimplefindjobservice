<?php
$params["__TITLE__"] = "Vacancies";

$regions = array();
$result = $conn->query("SELECT * FROM `region`;");
if ($result->num_rows > 0) {
	while ($row = $result->fetch_assoc()) {
		$regions[$row["id"]] = $row["name"];
	}
}
$specs = array();
$result = $conn->query("SELECT * FROM `specialization`;");
if ($result->num_rows > 0) {
	while ($row = $result->fetch_assoc()) {
		$specs[$row["id"]] = $row["name"];
	}
}

$vacancies_all = array();
$search_text = '';
$sql = "SELECT * FROM `vacancy`";
if (
	array_key_exists("search", $_GET)
	or array_key_exists("region", $_GET)
	or array_key_exists("salaryFrom", $_GET)
	or array_key_exists("salaryTo", $_GET)
) {
	$sql = $sql . " WHERE ";
	$isIfWrited = false;
	if (array_key_exists("search", $_GET)) {
		$search_text = $_GET["search"];
		$get_search = $conn->real_escape_string($_GET["search"]);
		$sql = $sql . "( title LIKE '%" . $get_search . "%' ";
		$sql = $sql . " OR description LIKE '%" . $get_search . "%' ";
		$sql = $sql . " OR description_short LIKE '%" . $get_search . "%' )";
		$isIfWrited = true;
	}
	if (array_key_exists("salaryFrom", $_GET)) {
		$get_salaryFrom = $conn->real_escape_string($_GET["salaryFrom"]);
		if ($isIfWrited) {
			$sql = $sql . " AND ";
		}
		$sql = $sql . "( salary_min >= " . $get_salaryFrom . ")";
		$isIfWrited = true;
	}
	if (array_key_exists("salaryTo", $_GET)) {
		$get_salaryTo = $conn->real_escape_string($_GET["salaryTo"]);
		if ($isIfWrited) {
			$sql = $sql . " AND ";
		}
		$sql = $sql . "( salary_max <= " . $get_salaryTo . ")";
		$isIfWrited = true;
	}
	if (array_key_exists("region", $_GET)) {
		$get_region = $conn->real_escape_string($_GET["region"]);
		if ($isIfWrited) {
			$sql = $sql . " AND ";
		}
		$sql = $sql . "(region_id LIKE " . array_search($get_region, $regions) . ")";
		$isIfWrited = true;
	}
	if (array_key_exists("specs", $_GET)) {
		if ($isIfWrited) {
			$sql = $sql . " AND ";
		}
		$sql = $sql . "(";
		for ($i = 0; $i < count($_GET["specs"]); $i++) {
			$get_spec_i = $conn->real_escape_string($_GET["specs"][$i]);
			if ($i != 0)
				$sql = $sql . " OR ";
			$sql = $sql . "spec_id LIKE " . array_search($get_spec_i, $specs);
		}
		$sql = $sql . ")";
	}
}
if (array_key_exists("sort", $_GET)) {
	if ($_GET["sort"] == "date") {
		$sql = $sql . "ORDER BY date_create DESC";
	} else if ($_GET["sort"] == "salary") {
		$sql = $sql . "ORDER BY salary_max DESC";
	}
} else {
	$sql = $sql . "ORDER BY date_create DESC";
}
$sql = $sql . ";";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
	while ($row = $result->fetch_assoc()) {
		$vacancy = new Vacancy(
			(int) $row["id"],
			strip_tags($row["title"], $allowed_tags),
			strip_tags($row["description_short"], $allowed_tags),
			strip_tags($row["salary_min"] . $allowed_tags),
			strip_tags($row["salary_max"], $allowed_tags),
			strip_tags($row["company_name"], $allowed_tags)
		);
		$vacancy->create_date = $row["date_create"];
		$vacancy->region = $regions[$row["region_id"]];
		$vacancy->spec = $specs[$row["spec_id"]];
		array_push($vacancies_all, $vacancy);
	}
}

if (!array_key_exists("page", $_GET)) {
	$_GET = array_merge($_GET, array("page" => 1));
}
if ($_GET["page"] < 1) {
	$_GET["page"] = 1;
}
$k = 3;
$max_k = ceil(count($vacancies_all) / $k);
if ($_GET["page"] > $max_k) {
	$_GET["page"] = $max_k;
}
$vacancies = array_slice(
	$vacancies_all, ($_GET["page"] - 1) * $k,
	$k
);

?>

<script>
	var tags = {};
	var sortType = "";
	function addTagsHandler(key, value) {
		tags[key] = value;
		updateTags();
	}
	function deleteTagsHandler(key) {
		delete tags[key];
		updateTags();
	}
	function updateTags() {
		console.log("Tags now: ", tags);
		var tagsBlock = document.getElementById("tags");
		tagsBlock.textContent = "";
		for (var key in tags) {
			var tagHTML = document.createElement("div");
			tagHTML.setAttribute("class", "search-tag");
			tagHTML.setAttribute("id", "tag_" + key);
			const keyDelete = key;
			tagHTML.onclick = function () { deleteTagsHandler(keyDelete); };
			tagHTML.textContent = tags[key];
			tagsBlock.appendChild(tagHTML);
		}
	}
	function setSort(type) {
		sortType = type;
		getSearch();
	}
	function getSearch() {
		search_string = "vacancies?"
		search_string += "search=" + document.getElementById("searchText").value;
		if (sortType != "") search_string += "&sort=" + sortType;
		for (var key in tags) {
			if (key.slice(0, 4) == "spec") {
				search_string += "&specs[]=" + tags[key];
			} else {
				search_string += "&" + key + "=" + tags[key];
			}
		}
		location.href = search_string;
	}
</script>

<div class="main-container-vacancies">

	<?php
	include("Filters.php");
	?>

	<div id="vacancies-container" class="vacancies-container">
		<form style="padding: 0px; margin: 0px" id="serachForm" action="javascript:getSearch();">
			<div class="search-field">
				<input type="text" placeholder="Поиск" autofocus id="searchText" value=<?php echo $search_text; ?>>
				<button type="submit" style="margin-left: 10px;">Искать</button>
			</div>
		</form>
		<div class="search-tags-container" id="tags">
			<!-- current tags here -->
		</div>

		<div class="search-sort-container">
			<div class="search-sort-element" onclick="javascript:setSort('salary')">По доходу</div>
			<div class="search-sort-element" onclick="javascript:setSort('date')">Сначала новые</div>
		</div>

		<!-- search results -->
		<?php
		$params_vacancy = array(
			"__ID__" => -1,
			"__TITLE__" => "This is a title",
			"__TEXT__" => "This is a text",
			"__SALARY_FROM__" => "sd0",
			"__SALARY_TO__" => "sd1",
			"__COMPANY_NAME__" => "This is company name",
		);

		for ($i = 0; $i < count($vacancies); $i++) {
			$params_vacancy["__ID__"] = $vacancies[$i]->id;
			$params_vacancy["__TITLE__"] = $vacancies[$i]->title;
			$params_vacancy["__TEXT__"] = $vacancies[$i]->text;
			$params_vacancy["__SPEC__"] = $vacancies[$i]->spec;
			$params_vacancy["__SALARY_FROM__"] = $vacancies[$i]->salary_from . " RUB";
			$params_vacancy["__SALARY_TO__"] = $vacancies[$i]->salary_to . " RUB";
			$params_vacancy["__COMPANY_NAME__"] = $vacancies[$i]->company_name;
			$params_vacancy["__CREATE_DATE__"] = $vacancies[$i]->create_date;
			$params_vacancy["__REGION__"] = $vacancies[$i]->region;
			ob_start();
			include("VacanciesItem.html");
			$data = ob_get_clean();
			echo strtr($data, $params_vacancy);
		}
		if (count($vacancies) == 0) {
			echo "Nouthing found!";
		} else {
			echo "<div class='search-pages-container'>";
			for ($i = 1; $i < $max_k + 1; $i++) {
				if ($i == $_GET["page"]) {
					echo "<div class='search-page-selected'>$i</div>";
				} else {
					//$page_link = "location.href=" . "'" . http_build_query($params_url) . "'";
					$url_params = $_GET;
					$url_params["page"] = $i;
					$page_link = "location.href='vacancies?" . http_build_query($url_params) . "'";
					echo "<div class='search-page' onclick=\"$page_link\">$i</div>";
				}
			}
			echo "</div>";
		}
		?>
		<!-- /search results -->

	</div>

</div>

<script>
	<?php
	foreach ($_GET as $key => $val) {
		if ($key == "specs") {
			for ($i = 0; $i < count($val); $i++) {
				$spec_key = "spec_" . array_search($val[$i], $specs);
				echo "tags['$spec_key'] = '$val[$i]';";
				echo "\n";
			}
		} else
			if ($key == "salaryTo" or $key == "salaryFrom" or $key == "region") {
				echo "tags['$key'] = '$val';";
				echo "\n";
			}
	}
	?>
	updateTags();
</script>