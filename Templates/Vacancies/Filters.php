<style>
    /* remove arraows on input numbers */
    /* Firefox */
    input[type='number'] {
        -moz-appearance: textfield;
    }

    /* Chrome, Safari, Edge, Opera */
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
    }
</style>

<script>
    function changeSalaryFromHandler(value) {
        addTagsHandler("salaryFrom", value);
        document.getElementById("field_salaryFrom").value = "";
    }
    function changeSalaryToHandler(value) {
        addTagsHandler("salaryTo", value);
        document.getElementById("field_salaryTo").value = "";
    }
</script>

<div class="filters-container">
    <details class="filter">
        <summary>Доход</summary>
        <div class="filter-element">
            От
            <input style="margin-left: 10px; width: 100%;" type="number" min="1" max="999999999"
                onchange="changeSalaryFromHandler(this.value);" id="field_salaryFrom">
        </div>
        <div class="filter-element">
            До
            <input style="margin-left: 10px; width: 100%;" type="number" min="1" max="999999999"
                onchange="changeSalaryToHandler(this.value);" id="field_salaryTo">
        </div>
    </details>

    <details class="filter">
        <summary>Регион</summary>
        <?php
        if (count($regions) == 0) {
            echo "Nouthing found!";
        }
        foreach ($regions as $key => $val) {
            echo "<div class='filter-element' style='cursor: pointer;' onclick='addTagsHandler(\"region\", \"$val\")'>";
            echo $val;
            echo "</div>";
        }
        ?>
    </details>
    <details class="filter">
        <summary>Специализации</summary>
        <?php
        if (count($specs) == 0) {
            echo "Nouthing found!";
        }
        foreach ($specs as $key => $val) {
            echo "<div class='filter-element' style='cursor: pointer;' onclick='addTagsHandler(\"spec_$key\", \"$val\")'>";
            echo $val;
            echo "</div>";
        }
        ?>
    </details>
</div>