<?php
$params["__TITLE__"] = "Profile";
?>

<script>
	var token = localStorage.getItem('JWTToken');
	var requestOk = false;
	var myEmail = "";
	if (token != null) {
		fetch("<?php echo $uri_root; ?>" + "/api/profile", {
			method: 'GET',
			cache: 'no-cache',
			headers: { 'Content-Type': 'application/json', 'Authentication': token },
		}).then((response) => {
			if (response.ok) {
				requestOk = true;
			} else {
				location.href = "/auth"
			}
			return response.json();
		})
			.then((data) => {
				if (requestOk) {
					myEmail = data["Email"];
					var nicknameBlock = document.getElementById("nickname");
					nicknameBlock.textContent = data["Nickname"];
					var descBlock = document.getElementById("description");
					if (data["Description"] == null || data["Description"] == "") {
						descBlock.textContent = "Пару слов о себе?";
					} else {
						descBlock.textContent = data["Description"];
					}
				}
				console.log(data);
			});

		var favouritesTitle = null;
		var favourites = null;
		fetch("<?php echo $uri_root; ?>" + "/api/favourites", {
			method: 'GET',
			cache: 'no-cache',
			headers: { 'Content-Type': 'application/json', 'Authentication': token },
		}).then((response) => {
			if (response.ok) {
				requestOk = true;
			}
			return response.json();
		})
			.then((data) => {
				if (requestOk) {
					favouritesTitle = data["FavouritesTitles"];
					favourites = data["Favourites"];
					favouritesBlock = document.getElementById("favourites");
					for (let i = 0; i < favouritesTitle.length; i++) {
						var favourElement = document.createElement("div");
						favourElement.setAttribute("class", "favorite");
						favourElement.setAttribute("onclick", "location.href='vacancy?id=" + favourites[i] + "'");
						favourElement.textContent = favouritesTitle[i];
						favouritesBlock.appendChild(favourElement);
					}
				}
				console.log(favouritesTitle);
			});
	} else {
		location.href = "/auth"
	}

	var nickname = "";
	var description = "";
	var requestOk = false;
	function nicknameHandler(value) {
		nickname = value;
	}
	function descriptionHandler(value) {
		description = value;
	}
	function saveChange() {
		var token = localStorage.getItem('JWTToken');
		var nicknameBlock = document.getElementById("nickname");
		var descBlock = document.getElementById("description");
		var data = {
			"Email": myEmail,
			"Nickname": nicknameBlock.value,
			"Description": descBlock.value
		};
		fetch("<?php echo $uri_root; ?>" + "/api/profile", {
			method: 'POST',
			cache: 'no-cache',
			headers: { 'Content-Type': 'application/json', 'Authentication': token },
			body: JSON.stringify(data)
		}).then((response) => {
			if (response.ok) {
				requestOk = true;
			}
			return response.json();
		})
			.then((data) => {
				if (requestOk) {
					location.href = "";
				}
				console.log(data);
			});
	}
	function setMode_editProfile() {
		var containerBlock = document.getElementById("titleContainer");
		var buttonBlockCancel = document.createElement("button");
		buttonBlockCancel.setAttribute("onclick", "setMode_viewProfile();");
		buttonBlockCancel.textContent = "Отменить";
		containerBlock.appendChild(buttonBlockCancel);

		var buttonBlockSave = document.getElementById("editButton");
		buttonBlockSave.textContent = "Сохранить";
		buttonBlockSave.setAttribute("onclick", "saveChange();");
		buttonBlockSave.setAttribute("style", "margin: 0px 5px");

		var nicknameBlock = document.getElementById("nickname");
		var newNicknameBlock = document.createElement("input");
		newNicknameBlock.setAttribute("class", "title");
		newNicknameBlock.setAttribute("style", "min-width: 200px; background-color: #ffffff; border: 1px solid #000000");
		newNicknameBlock.setAttribute("value", nicknameBlock.textContent);
		newNicknameBlock.setAttribute("placeholder", "Ваш никнейм");
		newNicknameBlock.setAttribute("id", "nickname");
		newNicknameBlock.innerHTML = nicknameBlock.innerHTML;
		nicknameBlock.parentElement.replaceChild(newNicknameBlock, nicknameBlock);

		var descBlock = document.getElementById("description");
		var newDescBlock = document.createElement("textarea");
		newDescBlock.setAttribute("type", "text");
		newDescBlock.setAttribute("class", "text-input");
		newDescBlock.setAttribute("placeholder", "Пару слов о себе");
		newDescBlock.setAttribute("id", "description");
		newDescBlock.innerHTML = descBlock.innerHTML;
		descBlock.parentElement.replaceChild(newDescBlock, descBlock);
	}
	function setMode_viewProfile() {
		location.href = "";
	}
</script>

<div class="main-container-profile">
	<div class="favorites-container" id="favourites">
		<div class="title">Избранное</div>
	</div>

	<div class="details-profile-container">
		<div class="details-profile">
			<div class="title-container" id="titleContainer">
				<span class="title" id="nickname">Ы</span><button onclick="setMode_editProfile();"
					id="editButton">Редактировать профиль</button>
			</div>
			<div class="text" id="description">Ы_Ы</div>
		</div>
	</div>
</div>