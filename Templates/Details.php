<?php
$regions = array();
$result = $conn->query("SELECT * FROM `region`;");
if ($result->num_rows > 0) {
	while ($row = $result->fetch_assoc()) {
		$regions[$row["id"]] = $row["name"];
	}
}
$specs = array();
$result = $conn->query("SELECT * FROM `specialization`;");
if ($result->num_rows > 0) {
	while ($row = $result->fetch_assoc()) {
		$specs[$row["id"]] = $row["name"];
	}
}

$vacancy = new Vacancy((int) $_GET["id"], "", "", "", "", "");

$result = $conn->query("SELECT * FROM `vacancy` WHERE id like $vacancy->id;");
if ($result->num_rows > 0) {
	while ($row = mysqli_fetch_assoc($result)) {
		$vacancy->id = $row["id"];
		$vacancy->title = strip_tags($row["title"], $allowed_tags);
		$vacancy->text_short = strip_tags($row["description_short"], $allowed_tags);
		$vacancy->text = strip_tags($row["description"], $allowed_tags);
		$vacancy->spec = strip_tags($specs[$row["spec_id"]], $allowed_tags);
		$vacancy->region = strip_tags($regions[$row["region_id"]], $allowed_tags);
		$vacancy->salary_from = strip_tags($row["salary_min"], $allowed_tags);
		$vacancy->salary_to = strip_tags($row["salary_max"], $allowed_tags);
		$vacancy->company_name = strip_tags($row["company_name"], $allowed_tags);
		$vacancy->create_date = strip_tags($row["date_create"], $allowed_tags);
	}
}

$params["__TITLE__"] = "Details: " . $vacancy->title;
?>

<div class="main-container-details">
	<?php if ($vacancy->title != ""): ?>
		<div class="details-container">
			<div class="details">
				<div class="title-container">
					<span class="title">
						<?php echo $vacancy->title; ?>
					</span>
					<button onclick="addToFavourites('POST')" id="addButton">В избранное</button>
					<button style="background-color: #b3b3b3;" onclick="addToFavourites('DELETE')" id="deleteButton">Удалить
						из избранного</button>
				</div>

				<div class="title">
					<?php echo $vacancy->company_name; ?>
				</div>
				<!-- <div class="logo"></div> -->

				<div class="text">
					<?php echo $vacancy->text; ?>
				</div>
				<div class="region">
					<?php echo $vacancy->spec; ?>
				</div>
				<div class="region">
					<?php echo $vacancy->region; ?>
				</div>
				<div class="salary">
					<?php echo $vacancy->salary_from; ?> RUB
				</div>
				<div class="salary">
					<?php echo $vacancy->salary_to; ?> RUB
				</div>
				<div style="padding: 5px">
					<?php echo $vacancy->create_date; ?>
				</div>
			</div>
		</div>
	<?php else: ?>
		<script>location.href = "/404"</script>
	<?php endif; ?>
</div>

<script>
	var token = localStorage.getItem('JWTToken');
	console.log(token);
	if (token == null) {
		document.getElementById("addButton").remove();
		document.getElementById("deleteButton").remove();
	} else {
		var favourites = null;
		var favouritesTitles = null;
		var requestOk = false;
		fetch("<?php echo $uri_root; ?>" + "/api/favourites", {
			method: 'GET',
			cache: 'no-cache',
			headers: { 'Content-Type': 'application/json', 'Authentication': token },
		}).then((response) => {
			if (response.ok) {
				requestOk = true;
			}
			return response.json();
		})
			.then((data) => {
				if (requestOk) {
					favourites = data["Favourites"];
					favouritesTitles = data["FavouritesTitles"];
					if (favourites.includes(<?php echo $vacancy->id; ?>)) {
						document.getElementById("addButton").remove();
					} else {
						document.getElementById("deleteButton").remove();
					}
				} else {
					document.getElementById("addButton").remove();
					document.getElementById("deleteButton").remove();
				}
			});
	}

	function addToFavourites(method_) {
		var requestOk = false;
		var token = localStorage.getItem('JWTToken');
		var data = {
			"vacancy_id": "<?php echo $vacancy->id; ?>",
		};
		fetch("<?php echo $uri_root; ?>" + "/api/favourites", {
			method: method_,
			cache: 'no-cache',
			headers: { 'Content-Type': 'application/json', 'Authentication': token },
			body: JSON.stringify(data)
		}).then((response) => {
			if (response.ok) {
				requestOk = true;
			}
			return response.json();
		})
			.then((data) => {
				if (requestOk) {
					location.href = "";
				}
				console.log(data);
			});
	}
</script>