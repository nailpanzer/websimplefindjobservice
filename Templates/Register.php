<?php
$params["__TITLE__"] = "Register";
?>

<script>
    let email = "";
    let nickname = "";
    let password1 = "";
    let password2 = "";
    function emailHandler(value) {
        email = value;
    }
    function nicknameHandler(value) {
        nickname = value;
    }
    function password1Handler(value) {
        password1 = value;
    }
    function password2Handler(value) {
        password2 = value;
    }
    function getRegister() {
        regFormMessage = document.getElementById("register_form_message");
        regFormMessage.textContent = "";
        if (password1 != password2) {
            regFormMessage.textContent = "Пароли не совпадают";
        } else {
            console.log("email: " + email);
            console.log("nickname: " + nickname);
            console.log("password1: " + password1);
            data = {
                "email": email,
                "nickname": nickname,
                "password": password1
            };
            fetch("<?php echo $uri_root; ?>" + "/api/register", {
                method: 'POST',
                cache: 'no-cache',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(data)
            }).then((response) => {
                if (response.ok) {
                    location.href = "auth";
                }
                return response.json();
            })
                .then((data) => {
                    regFormMessage.textContent = data["Message"];
                });
        }
    }
</script>

<div class="main-container-auth">
    <form id="register_form" action="javascript:getRegister();">
        <input class="auth-input-field" placeholder="Email" onchange="emailHandler(this.value);">
        <input class="auth-input-field" placeholder="Никнейм" onchange="nicknameHandler(this.value);">
        <input class="auth-input-field" type="password" placeholder="Пароль" onchange="password1Handler(this.value);">
        <input class="auth-input-field" type="password" placeholder="ПовторитеПароль"
            onchange="password2Handler(this.value);">
        <button class="submit-auth-button" type="submit">Регистрация</button>
        <div id="register_form_message" style="padding: 10px 0px; text-align: center;"></div>
        <form>
</div>