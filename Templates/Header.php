<style>
	.header-button {
		margin: 0px 0px 0px 10px;
	}
</style>

<div class="h-main-container-header-footer">
	<a href="/"><img src="/favicon.ico" class="h-logo" style="cursor: pointer;" /></a>
	<div class="h-right-container" id="header">
		<script>
			function logout() {
				localStorage.removeItem("JWTToken");
				location.href = "";
			}

			let headerBlock = document.getElementById("header");
			if (localStorage.getItem("JWTToken") == null) {
				let but1 = document.createElement("button");
				but1.setAttribute("class", "header-button");
				but1.setAttribute("onclick", "location.href='/auth'");
				but1.textContent = "Вход";
				let but2 = document.createElement("button");
				but2.setAttribute("class", "header-button");
				but2.setAttribute("onclick", "location.href='/register'");
				but2.textContent = "Регистрация";
				headerBlock.appendChild(but1);
				headerBlock.appendChild(but2);
			} else {
				let but1 = document.createElement("button");
				but1.setAttribute("class", "header-button");
				but1.setAttribute("onclick", "location.href='/profile'");
				but1.textContent = "Профиль";
				headerBlock.appendChild(but1);

				let but2 = document.createElement("button");
				but2.setAttribute("class", "header-button");
				but2.setAttribute("onclick", "logout();");
				but2.textContent = "Выход";
				headerBlock.appendChild(but2);

				// let but3 = document.createElement("button");
				// but3.setAttribute("class", "header-button");
				// but3.setAttribute("onclick", "location.href='/admin'");
				// but3.textContent = "Администрирование";
				// headerBlock.appendChild(but3);
			}
		</script>

	</div>
</div>