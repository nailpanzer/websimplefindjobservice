<?php
$params = array_merge($params, array(
    "__TITLE__" => "HAVE NO TITLE",
)
);
?>

<!DOCTYPE html>
<html lang="ru">

<head>
    <meta content='text/html' charset='utf8'>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>__TITLE__</title>
    <link rel="stylesheet" href="../Styles/Global.css">
    <link rel="stylesheet" href="../Styles/HeaderFooter.css">
    <link rel="stylesheet" href="../Styles/Vacancies.css">
    <link rel="stylesheet" href="../Styles/Details.css">
    <link rel="stylesheet" href="../Styles/Profile.css">
    <link rel="stylesheet" href="../Styles/Auth.css">
    <link rel="stylesheet" href="../Styles/Admin.css">
</head>

<body>
    <div class="main-container">
        <?php
        include("Header.php");
        ?>

        <div class="main-content-container">
            <?php
            include($data_file); // content will be here
            ?>
        </div>

        <?php
        include("Footer.html");
        ?>
    </div>
</body>

</html>