<?php
$params["__TITLE__"] = "Authorization";
?>

<script>
    let email = "";
    let password = "";
    function emailHandler(value) {
        email = value;
    }
    function passwordHandler(value) {
        password = value;
    }
    function getAuth() {
        regFormMessage = document.getElementById("auth_form_message");
        regFormMessage.textContent = "";
        requestOk = false;
        data = {
            "email": email,
            "password": password
        };
        fetch("<?php echo $uri_root; ?>" + "/api/auth", {
            method: 'POST',
            cache: 'no-cache',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(data)
        }).then((response) => {
            if (response.ok) {
                requestOk = true;
            }
            return response.json();
        })
            .then((data) => {
                regFormMessage.textContent = data["Message"];
                if (requestOk) {
                    localStorage.setItem("JWTToken", data["Token"]);
                    location.href = "/";
                }
            });
    }
</script>

<div class="main-container-auth">
    <form id="auth_form" action="javascript:getAuth();">
        <input class="auth-input-field" placeholder="Email" onchange="emailHandler(this.value);">
        <input class="auth-input-field" type="password" placeholder="Пароль" onchange="passwordHandler(this.value);">
        <button class="submit-auth-button" type="submit">Вход</button>
        <div id="auth_form_message" style="padding: 10px 0px; text-align: center;"></div>
    </form>
</div>