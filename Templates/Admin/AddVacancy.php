<?php
$params["__TITLE__"] = "Создание вакансии";

$specs = array();
$result = $conn->query("SELECT * FROM `specialization`;");
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $specs[$row["id"]] = $row["name"];
    }
}

$regions = array();
$result = $conn->query("SELECT * FROM `region`;");
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $regions[$row["id"]] = $row["name"];
    }
}

?>

<style type="text/css">
    .add-content-container {
        margin: 0px;
        padding: 5px;

        background-color: #F0F0F0;

        display: flex;
        flex-direction: column;
    }
</style>

<div class="add-content-container">
    <div class="title" id="admin-edit-title">Создание вакансии</div>
    <div class="field-container">
        <div class="field-name">Заголовок</div>
        <input id="title" class="admin-input" type="text" placeholder="Введите заголовок вакансии (до 63 символов)"
            maxlength="63" onchange="titleHandler(this.value)"></input>
    </div>

    <div class="field-container">
        <div class="field-name">Короткое описание</div>
        <textarea id="short-desc" type="text" oninput="auto_grow(this)"
            placeholder="Введите короткое описание вакансии (до 255 символов)" maxlength="253"
            onchange="descriptionShortHandler(this.value)"></textarea>
    </div>

    <div class="field-container">
        <div class="field-name">Описание</div>
        <textarea id="desc" type="text" oninput="auto_grow(this)" placeholder="Введите полное описание вакансии"
            onchange="descriptionHandler(this.value)"></textarea>
    </div>

    <div class="field-container">
        <div class="field-name">Название компании</div>
        <input id="company" class="admin-input" placeholder="Введите название компании работодателя"
            onchange="companyNameHandler(this.value)">
    </div>

    <div class="field-container">
        <div class="field-name">Регион</div>
        <details class="list-container">
            <summary>Выбрать</summary>
            <?php
            if (count($regions) == 0) {
                echo "Nouthing found!";
            }
            foreach ($regions as $key => $val) {
                echo "<div class='list-elem' onclick='regionHandler($key)'>";
                echo $val;
                echo "</div>";
            }
            ?>
        </details>
    </div>

    <div class="field-container">
        <div class="field-name">Специализация</div>
        <details class="list-container">
            <summary>Выбрать</summary>
            <?php
            if (count($specs) == 0) {
                echo "Nouthing found!";
            }
            foreach ($specs as $key => $val) {
                echo "<div class='list-elem' onclick='specIdHandler($key)'>";
                echo $val;
                echo "</div>";
            }
            ?>
        </details>
    </div>

    <div class="field-container">
        <div class="field-name">Заработная плата</div>
        <div class="row-container">
            <input id="salary-min" class="admin-input" type="number" placeholder="Минимальная" maxlength="63"
                style="margin-right: 10px" onchange="salaryMinHandler(this.value)">
            <input id="salary-max" class="admin-input" type="number" placeholder="Максимальная" maxlength="63"
                onchange="salaryMaxHandler(this.value)">
        </div>
    </div>

    <div style="display: flex; flex-direction: row;">
        <button style="margin: 5px;" onclick="addVacancy('POST');" id="apply-button">Сохранить</button>
        <div id="message_add" style="padding: 10px 0px; text-align: center;"></div>
        <div id="confirm-container" style="margin-left: auto; text-align: right"></div>
    </div>

</div>

<script>
    function auto_grow(element) {
        element.style.height = "5px";
        element.style.height = (element.scrollHeight) + "px";
    }

    let title = "";
    let description = "";
    let description_short = "";
    let region_id = "";
    let spec_id = "";
    let salary_min = "";
    let salary_max = "";
    let company_name = "";

    function titleHandler(value) {
        let msgElem = document.getElementById("message_add");
        msgElem.textContent = "";
        title = value;
    }
    function descriptionHandler(value) {
        let msgElem = document.getElementById("message_add");
        msgElem.textContent = "";
        description = value;
    }
    function descriptionShortHandler(value) {
        let msgElem = document.getElementById("message_add");
        msgElem.textContent = "";
        description_short = value;
    }
    function regionHandler(value) {
        let msgElem = document.getElementById("message_add");
        msgElem.textContent = "";
        region_id = value;
    }
    function specIdHandler(value) {
        let msgElem = document.getElementById("message_add");
        msgElem.textContent = "";
        spec_id = value;
    }
    function salaryMinHandler(value) {
        let msgElem = document.getElementById("message_add");
        msgElem.textContent = "";
        salary_min = value;
    }
    function salaryMaxHandler(value) {
        let msgElem = document.getElementById("message_add");
        msgElem.textContent = "";
        salary_max = value;
    }
    function companyNameHandler(value) {
        let msgElem = document.getElementById("message_add");
        msgElem.textContent = "";
        company_name = value;
    }

    function addVacancy(req_type) {
        let requestOk = false;
        let data_in = {
            "title": title,
            "description": description,
            "description_short": description_short,
            "region_id": region_id,
            "spec_id": spec_id,
            "salary_min": salary_min,
            "salary_max": salary_max,
            "company_name": company_name,
        };
        fetch("<?php echo $uri_root; ?>" + "/api/vacancies", {
            method: req_type,
            cache: 'no-cache',
            headers: { 'Content-Type': 'application/json', 'Authentication': localStorage.getItem('JWTToken') },
            body: JSON.stringify(data_in)
        }).then((response) => {
            if (response.ok) {
                requestOk = true;
                location.href = "/admin";
            }
            return response.json();
        })
            .then((data) => {
                msgElem = document.getElementById("message_add");
                if (requestOk) {
                    console.log(data)
                    msgElem.textContent = data['Message'];
                } else {
                    msgElem.textContent = data['Message'];
                }
            });
    }

</script>