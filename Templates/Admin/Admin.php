<?php
$params['__TITLE__'] = "Администрирование";
?>

<div id="message" style="padding: 10px 0px; text-align: center;"></div>

<div id="admin-container" class="admin-container">
    <div class="admin-side-container">
        <div class="title">Страницы</div>
        <div class="admin-side-element" onclick="getListItems('/api/vacancies', writerVacancy);">Вакансии</div>
        <div class="admin-side-element" onclick="window.open('/admin/add-vacancy', '_blank');">Новая вакансия</div>
        <div class="admin-side-element" onclick="getListItems('/api/specs', writerSpecs);">Специализации</div>
        <div class="admin-side-element" onclick="window.open('/admin/add-spec', '_blank');">Новая специализация</div>
    </div>
    <div class="admin-edit-container">
        <form style="padding: 0px; margin: 0px; display: none;" id="serachForm" action="javascript:getSearch();">
            <div class="search-field">
                <input type="text" placeholder="Поиск" autofocus id="searchText">
                <button type="submit" style="margin-left: 10px;">Искать</button>
            </div>
        </form>
        <div class="admin-edit" id="admin-content-container">Контент</div>
        <div class="search-pages-container" id="pages-container"></div>
    </div>
</div>

<script defer>
    var token = localStorage.getItem('JWTToken');
    let requestOk = false;
    let page = 1;
    fetch("<?php echo $uri_root; ?>" + "/api/admin", {
        method: 'GET',
        cache: 'no-cache',
        headers: { 'Content-Type': 'application/json', 'Authentication': token },
    }).then((response) => {
        if (response.ok) {
            requestOk = true;
        }
        return response.json();
    })
        .then((data) => {
            if (requestOk) {
                let adminContainer = document.getElementById("admin-container");
                adminContainer.style.display = "flex";
                let mesElem = document.getElementById("message");
                mesElem.style.display = "None";
            } else {
                let mesElem = document.getElementById("message");
                mesElem.textContent = data["Message"];
            }
            console.log(data);
        });


    let current_path = "/api/vacancies";
    let current_writer = writerVacancy;
    function getSearch() {
        let params = "search=" + document.getElementById("searchText").value;
        getListItems(current_path, current_writer, params);
    }
    // get list of some data from api
    // and write it in html by writer
    function getListItems(path, writer, params = "") {
        document.getElementById("serachForm").style.display = "none";
        content_path = path;
        current_writer = writer;
        let content_field = document.getElementById("admin-content-container");
        let content_pages_container = document.getElementById("pages-container");
        content_pages_container.innerHTML = null;
        content_field.innerHTML = null;
        var token = localStorage.getItem('JWTToken');
        let requestOk = false;
        fetch("<?php echo $uri_root; ?>" + path + "?page=" + page + "&" + params, {
            method: 'GET',
            cache: 'no-cache',
            headers: { 'Content-Type': 'text/html', 'Authentication': token },
        }).then((response) => {
            if (response.ok) {
                requestOk = true;
            }
            return response.json();
        })
            .then((data) => {
                if (requestOk) {
                    writer(data);
                    for (let i = 0; i < data["Max_pages"]; i++) {
                        let page_block = document.createElement("div");
                        if (i + 1 == data["Page"]) {
                            page_block.setAttribute("class", "search-page-selected");
                        } else {
                            page_block.setAttribute("class", "search-page");
                            page_block.onclick = function () {
                                page = i + 1;
                                getListItems(path, writer);
                            };
                        }
                        page_block.textContent = i + 1;
                        content_pages_container.appendChild(page_block);
                    }
                }
                console.log(data);
            });
    }

    // it gets list of vacancies and writes in code
    function writerVacancy(data) {
        document.getElementById("serachForm").style.display = "block";
        let content_field = document.getElementById("admin-content-container");
        vacancies = data["Vacancies"];
        for (let i = 0; i < vacancies.length; i++) {
            let content_a = document.createElement("a");
            let content_item = document.createElement("div");
            content_item.setAttribute("class", "admin-content-element");
            content_a.setAttribute("href", "/admin/edit-vacancy?id=" + vacancies[i]["id"]);
            content_item.textContent = vacancies[i]["id"] + ": " + vacancies[i]["title"];
            content_a.appendChild(content_item);
            content_field.appendChild(content_a);
        }
        console.log("Vacancies writed!");
    }
    function writerSpecs(data) {
        let content_field = document.getElementById("admin-content-container");
        specs = data["Specs"];
        for (let i = 0; i < specs.length; i++) {
            let content_a = document.createElement("a");
            let content_item = document.createElement("div");
            content_item.setAttribute("class", "admin-content-element");
            content_a.setAttribute("href", "/admin/edit-spec?id=" + specs[i]["id"]);
            content_item.textContent = specs[i]["id"] + ": " + specs[i]["name"];
            content_a.appendChild(content_item);
            content_field.appendChild(content_a);
        }
        console.log("Specs writed!");
    }

    // request to get some code should be inserted
    function getHTMLContent(path) {
        content_field = document.getElementById("admin-content-container");
        content_field.innerHTML = null;
        var token = localStorage.getItem('JWTToken');
        let requestOk = false;
        fetch("<?php echo $uri_root; ?>" + path, {
            method: 'GET',
            cache: 'no-cache',
            headers: { 'Content-Type': 'text/html', 'Authentication': token },
        }).then((response) => {
            if (response.ok) {
                requestOk = true;
            }
            return response.text();
        })
            .then((data) => {
                if (requestOk) {
                    content_field = document.getElementById("admin-content-container");
                    content_field.innerHTML = data;
                    // if only use content_field.innerHTML = data then scripts will be inserted, but not active
                    // so, we need to make something like this
                    var data_scripts = document.querySelectorAll("#admin-content-container > script")
                    for (let i = 0; i < data_scripts.length; i++) {
                        sc_block = document.createElement("script");
                        sc_block.textContent = data_scripts[i].innerHTML;
                        content_field.appendChild(sc_block)
                    }
                    console.log("Loaded \'" + path + "\'");
                } else {
                    let mesElem = document.getElementById("message");
                    mesElem.textContent = data["Message"];
                    console.log("Some load error \'" + path + "\'. Error: " + data["Message"]);
                }

            });
    }

    getListItems('/api/vacancies', writerVacancy);
</script>