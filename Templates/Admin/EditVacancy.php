<?php
$http_body = json_decode(file_get_contents('php://input'), true);
include("AddVacancy.php");
$params["__TITLE__"] = "Редактирование вакансии";

if (!array_key_exists("id", $_GET)) {
    header('Location: ' . $uri_root . "/404");
}

?>

<script>
    let edit_title = document.getElementById("admin-edit-title");
    edit_title.textContent = "Редактирование вакансии";
    let apply_button = document.getElementById("apply-button");
    apply_button.setAttribute("onClick", "applyChanges()");
    let confirm_container = document.getElementById("confirm-container");
    let delete_button = document.createElement("button");
    delete_button.id = "delete-button";
    delete_button.textContent = "Удалить!";
    delete_button.setAttribute("onClick", "showDeleteConfirm()");
    confirm_container.appendChild(delete_button);

    var vacancy = null;
    var token = localStorage.getItem('JWTToken');
    let requestOk = false;
    fetch("<?php echo $uri_root; ?>" + "/api/vacancies?id=" + <?php echo $_GET["id"]; ?>, {
        method: 'GET',
        cache: 'no-cache',
        headers: { 'Content-Type': 'application/json', 'Authentication': token },
    }).then((response) => {
        if (response.ok) {
            requestOk = true;
        }
        return response.json();
    })
        .then((data) => {
            msgElem = document.getElementById("message_add");
            if (requestOk) {
                writeVacancyData(data);
            }
            console.log(data);
        });

    // writes data in html code
    function writeVacancyData(data) {
        vacancy = data["Vacancy"];

        titleHandler(vacancy["title"]);
        descriptionHandler(vacancy["text"]);
        descriptionShortHandler(vacancy["text_short"]);
        regionHandler(vacancy["region_id"]);
        specIdHandler(vacancy["spec_id"]);
        salaryMinHandler(vacancy["salary_from"]);
        salaryMaxHandler(vacancy["salary_to"]);
        companyNameHandler(vacancy["company_name"]);

        document.getElementById("title").value = title;
        document.getElementById("short-desc").value = description_short;
        document.getElementById("desc").value = description;
        document.getElementById("company").value = company_name;
        document.getElementById("salary-min").value = salary_min;
        document.getElementById("salary-max").value = salary_max;
    }

    // save
    function applyChanges() {
        let requestOk = false;
        let data_in = {
            "id": "<?php echo $_GET["id"]; ?>",
            "title": title,
            "description": description,
            "description_short": description_short,
            "region_id": region_id,
            "spec_id": spec_id,
            "salary_min": salary_min,
            "salary_max": salary_max,
            "company_name": company_name,
        };
        fetch("<?php echo $uri_root; ?>" + "/api/vacancies", {
            method: "PUT",
            cache: 'no-cache',
            headers: { 'Content-Type': 'application/json', 'Authentication': localStorage.getItem('JWTToken') },
            body: JSON.stringify(data_in)
        }).then((response) => {
            if (response.ok) {
                requestOk = true;
            }
            return response.json();
        })
            .then((data) => {
                console.log(data);
                msgElem = document.getElementById("message_add");
                msgElem.textContent = data["Message"];
                if (requestOk) {
                    window.open("/vacancy?id=" + data_in["id"]);
                }
            });
    }

    // show confirm window and delete button
    function showDeleteConfirm() {
        let confirm_container = document.getElementById("confirm-container");
        let delete_button = document.getElementById("delete-button");

        // confirm delete button
        if (document.getElementById("confirm-delete-button") == null) {
            let confirm_button = document.createElement("button");
            confirm_button.setAttribute("onClick", "confirmDelete()");
            confirm_button.setAttribute("style", "margin-left: 5px");
            confirm_button.id = "confirm-delete-button";
            confirm_button.textContent = "Подтвердить удаление";
            confirm_container.appendChild(confirm_button);
        }
    }

    // request to delete
    function confirmDelete() {
        var token = localStorage.getItem('JWTToken');
        let requestOk = false;
        let data_in = {
            "id": <?php echo $_GET["id"]; ?>,
        }
        fetch("<?php echo $uri_root; ?>" + "/api/vacancies?id=" + <?php echo $_GET["id"]; ?>, {
            method: 'DELETE',
            cache: 'no-cache',
            headers: { 'Content-Type': 'application/json', 'Authentication': token },
            body: JSON.stringify(data_in),
        }).then((response) => {
            if (response.ok) {
                requestOk = true;
            }
            return response.json();
        })
            .then((data) => {
                msgElem = document.getElementById("message_add");
                msgElem.textContent = data["Message"];
                if (requestOk) {
                    window.open("/admin", "_self");
                }
                console.log(data);
            });
    }
</script>