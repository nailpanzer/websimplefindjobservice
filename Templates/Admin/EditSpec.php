<?php
$http_body = json_decode(file_get_contents('php://input'), true);
include("AddSpec.php");
$params["__TITLE__"] = "Редактирование специализации";

if (!array_key_exists("id", $_GET)) {
    header('Location: ' . $uri_root . "/404");
}

?>

<script>
    let edit_title = document.getElementById("admin-edit-title");
    edit_title.textContent = "Редактирование специализации";
    let apply_button = document.getElementById("apply-button");
    apply_button.setAttribute("onClick", "applyChanges()");

    var spec = null;
    var token = localStorage.getItem('JWTToken');
    let requestOk = false;
    fetch("<?php echo $uri_root; ?>" + "/api/specs?id=" + <?php echo $_GET["id"]; ?>, {
        method: 'GET',
        cache: 'no-cache',
        headers: { 'Content-Type': 'application/json', 'Authentication': token },
    }).then((response) => {
        if (response.ok) {
            requestOk = true;
        }
        return response.json();
    })
        .then((data) => {
            msgElem = document.getElementById("message_add");
            if (requestOk) {
                writeSpecData(data);
            }
            console.log(data);
        });

    // writes data in html code
    function writeSpecData(data) {
        spec = data["Spec"];

        nameHandler(spec["name"]);
        descriptionHandler(spec["desc"]);

        if (description == undefined) {
            description = "";
        }

        document.getElementById("name").value = name;
        document.getElementById("desc").value = description;
    }

    // save
    function applyChanges() {
        let requestOk = false;
        let data_in = {
            "id": "<?php echo $_GET["id"]; ?>",
            "name": name,
            "description": description,
        };
        fetch("<?php echo $uri_root; ?>" + "/api/specs", {
            method: "PUT",
            cache: 'no-cache',
            headers: { 'Content-Type': 'application/json', 'Authentication': localStorage.getItem('JWTToken') },
            body: JSON.stringify(data_in)
        }).then((response) => {
            if (response.ok) {
                requestOk = true;
            }
            console.log(response);
            return response.json();
        })
            .then((data) => {
                console.log(data);
                msgElem = document.getElementById("message_add");
                msgElem.textContent = data["Message"];
                if (requestOk) {
                    location.href = "/admin";
                }
            });
    }

</script>