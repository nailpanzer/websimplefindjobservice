<?php
$params["__TITLE__"] = "Создание специализации";

?>

<style type="text/css">
    .add-spec-container {
        margin: 0px;
        padding: 5px;

        background-color: #F0F0F0;

        display: flex;
        flex-direction: column;
    }
</style>

<div class="add-spec-container">
    <div class="title" id="admin-edit-title">Создание специализации</div>
    <div class="field-container">
        <div class="field-name">Имя</div>
        <input id="name" class="admin-input" type="text" placeholder="Введите название вспециализации (до 63 символов)"
            maxlength="63" onchange="nameHandler(this.value)"></input>
    </div>

    <div class="field-container">
        <div class="field-name">Описание</div>
        <textarea id="desc" type="text" oninput="auto_grow(this)" placeholder="Введите описание специализации"
            onchange="descriptionHandler(this.value)"></textarea>
    </div>

    <div style="display: flex; flex-direction: row;">
        <button style="margin: 5px;" onclick="addSpec();" id="apply-button">Сохранить</button>
        <div id="message_add" style="padding: 10px 0px; text-align: center;"></div>
        <div id="confirm-container" style="margin-left: auto; text-align: right"></div>
    </div>

</div>

<script>
    function auto_grow(element) {
        element.style.height = "5px";
        element.style.height = (element.scrollHeight) + "px";
    }

    let name = "";
    let description = "";

    function nameHandler(value) {
        let msgElem = document.getElementById("message_add");
        msgElem.textContent = "";
        name = value;
    }
    function descriptionHandler(value) {
        let msgElem = document.getElementById("message_add");
        msgElem.textContent = "";
        description = value;
    }

    function addSpec() {
        let requestOk = false;
        let data_in = {
            "name": name,
            "description": description,
        };
        console.log(localStorage.getItem('JWTToken'));
        fetch("<?php echo $uri_root; ?>" + "/api/specs", {
            method: "POST",
            cache: 'no-cache',
            headers: { 'Content-Type': 'application/json', 'Authentication': localStorage.getItem('JWTToken') },
            body: JSON.stringify(data_in)
        }).then((response) => {
            if (response.ok) {
                requestOk = true;
                location.href = "/admin";
            }
            return response.json();
        })
            .then((data) => {
                msgElem = document.getElementById("message_add");
                if (requestOk) {
                    console.log(data)
                    msgElem.textContent = data['Message'];
                } else {
                    msgElem.textContent = data['Message'];
                }
            });
    }

</script>