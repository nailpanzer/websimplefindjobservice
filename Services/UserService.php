<?php
class User
{
    public $id = 0;
    public $nickname = "";
    public $email = "";
    public $description = "";
    public $role = 0;
}

class UserService
{
    private static $secret = "qwerty";

    public static function getJWTToken()
    {
        $headers = getallheaders();
        if (array_key_exists("Authentication", $headers)) {
            return $headers["Authentication"];
        }
        return false;
    }

    public static function encodeToken($user)
    {
        // sha256 - encode method
        return hash_hmac("sha256", $user->email, UserService::$secret);
    }

    public static function decodeToken($token, $conn)
    {
        $result = $conn->query("SELECT * FROM user_info");
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $user = new User();
                $user->email = $row["email"];
                $token_user = UserService::encodeToken($user);
                if ($token_user == $token) {
                    $user->id = $row["id"];
                    $user->nickname = $row["nickname"];
                    $user->description = $row["description"];
                    $user->role = (int) $row["role"];
                    return $user;
                }
            }
        }
        return false;
    }

    public static function checkToken($user, $token)
    {
        $token_user = UserService::encodeToken($user);
        return $token_user == $token;
    }
}
?>