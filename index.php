<?php
function debug($data)
{
	$output = $data;
	if (is_array($output))
		$output = implode(', ', $output);
	echo "<script>console.log('DEBUG: " . $output . "' );</script>";
}

// disable error reports
//error_reporting(0);

$allowed_tags = "<div><br><p><a><li><ul><span><strong>";

$uri_root = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http")
	. "://" . $_SERVER["HTTP_HOST"];
if (!filter_var($uri_root . '/' . $_SERVER['REQUEST_URI'], FILTER_VALIDATE_URL)) {
	echo "Incorrect URL";
}
$uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$uri = trim($uri, '/');

ob_start();
$params = array(); // this array will be updating on templates
include("Api/Global.php");
include("Models/Models.php");
include("Services/UserService.php");
ob_end_clean();

// connecting to database
$dbhost = "localhost";
$dbuser = "root";
$dbpass = "";
$db = "db_findjob";
$conn = mysqli_connect($dbhost, $dbuser, $dbpass, $db);
mysqli_set_charset($conn, 'utf8');
$result = $conn->query("SET NAMES UTF8;");

// main render
ob_start();
debug("REQUEST_URI: " . $uri);
$data_file = "";
switch ($uri) {
	case "":
		header('Location: ' . $uri_root . "/vacancies");
		break;
	case "403":
		$data_file = "Error403.php";
		include("Templates/Template.php");
		break;
	case "vacancies":
		$data_file = "Vacancies/Vacancies.php";
		include("Templates/Template.php");
		break;
	case "vacancy":
		$data_file = "Details.php";
		include("Templates/Template.php");
		break;
	case "register":
		$data_file = "Register.php";
		include("Templates/Template.php");
		break;
	case "auth":
		$data_file = "Auth.php";
		include("Templates/Template.php");
		break;
	case "profile":
		$data_file = "Profile.php";
		include("Templates/Template.php");
		break;
	case "admin":
		$data_file = "Admin/Admin.php";
		include("Templates/Template.php");
		break;
	case "admin/add-vacancy":
		$data_file = "Admin/AddVacancy.php";
		include("Templates/Template.php");
		break;
	case "admin/edit-vacancy":
		$data_file = "Admin/EditVacancy.php";
		include("Templates/Template.php");
		break;
	case "admin/add-spec":
		$data_file = "Admin/AddSpec.php";
		include("Templates/Template.php");
		break;
	case "admin/edit-spec":
		$data_file = "Admin/EditSpec.php";
		include("Templates/Template.php");
		break;
	case "api/register":
		include("Api/RegisterController.php");
		break;
	case "api/auth":
		include("Api/AuthController.php");
		break;
	case "api/profile":
		include("Api/ProfileController.php");
		break;
	case "api/favourites":
		include("Api/FavouritesController.php");
		break;
	case "api/vacancies":
		include("Api/VacanciesController.php");
		break;
	case "api/specs":
		include("Api/SpecsController.php");
		break;
	case "api/admin":
		$_admin_req = "check";
		include("Api/AdminController.php");
		break;
	default:
		$data_file = "Error404.php";
		include("Templates/Template.php");
}
$page = ob_get_clean();

echo strtr($page, $params);
$conn->close();
exit;
?>