<?php
$http_body = json_decode(file_get_contents('php://input'), true);

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    // get vacancies

    $regions = array();
    $result = $conn->query("SELECT * FROM `region`;");
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $regions[$row["id"]] = $row["name"];
        }
    }
    $specs = array();
    $result = $conn->query("SELECT * FROM `specialization`;");
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $specs[$row["id"]] = $row["name"];
        }
    }

    // returns only one vacancy
    if (array_key_exists("id", $_GET)) {
        $vacancy = new Vacancy((int) $_GET["id"], "", "", "", "", "");
        $result = $conn->query("SELECT * FROM `vacancy` WHERE id like $vacancy->id;");
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $vacancy->id = $row["id"];
                $vacancy->title = strip_tags($row["title"], $allowed_tags);
                $vacancy->text_short = strip_tags($row["description_short"], $allowed_tags);
                $vacancy->text = strip_tags($row["description"], $allowed_tags);
                $vacancy->spec = strip_tags($specs[$row["spec_id"]], $allowed_tags);
                $vacancy->spec_id = strip_tags($row["spec_id"], $allowed_tags);
                $vacancy->region = strip_tags($regions[$row["region_id"]], $allowed_tags);
                $vacancy->region_id = strip_tags($row["region_id"], $allowed_tags);
                $vacancy->salary_from = strip_tags($row["salary_min"], $allowed_tags);
                $vacancy->salary_to = strip_tags($row["salary_max"], $allowed_tags);
                $vacancy->company_name = strip_tags($row["company_name"], $allowed_tags);
                $vacancy->create_date = strip_tags($row["date_create"], $allowed_tags);
            }
        }
        response(200, array("Message" => "Успех!", "Vacancy" => $vacancy));
    }
    // returns the found vacancies
    if (array_key_exists("search", $_GET)) {
        $vacancies_all = array();
        $search_text = $_GET["search"];
        $get_search = $conn->real_escape_string($_GET["search"]);

        $result = $conn->query("SELECT * FROM vacancy WHERE 
        (title LIKE '%" . $get_search . "%') 
        OR (description LIKE '%" . $get_search . "%') 
        OR (description_short LIKE '%" . $get_search . "%');");

        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $vacancy = new Vacancy(
                    (int) $row["id"],
                    strip_tags($row["title"], $allowed_tags),
                    strip_tags($row["description"], $allowed_tags),
                    strip_tags($row["salary_min"] . $allowed_tags),
                    strip_tags($row["salary_max"], $allowed_tags),
                    strip_tags($row["company_name"], $allowed_tags)
                );
                $vacancy->text_short = strip_tags($row["description_short"], $allowed_tags);
                $vacancy->spec = strip_tags($specs[$row["spec_id"]]);
                $vacancy->spec_id = strip_tags($row["spec_id"], $allowed_tags);
                $vacancy->create_date = strip_tags($row["date_create"]);
                $vacancy->region = strip_tags($regions[$row["region_id"]]);
                $vacancy->region_id = strip_tags($row["region_id"], $allowed_tags);
                array_push($vacancies_all, $vacancy);
            }
        }

        if (!array_key_exists("page", $_GET)) {
            $_GET = array_merge($_GET, array("page" => 1));
        }
        if ($_GET["page"] < 1) {
            $_GET["page"] = 1;
        }
        if (!array_key_exists("on-page", $_GET)) {
            $_GET = array_merge($_GET, array("on-page" => 12));
        }
        $k = $_GET["on-page"];
        $max_k = ceil(count($vacancies_all) / $k);
        if ($_GET["page"] > $max_k) {
            $_GET["page"] = $max_k;
        }
        $vacancies = array_slice(
            $vacancies_all, ($_GET["page"] - 1) * $k,
            $k
        );

        response(200, array("Message" => "Успех!", "Vacancies" => $vacancies, "Page" => $_GET["page"], "Max_pages" => $max_k));
    }
    // returns all vacancies
    $vacancies_all = array();
    $result = $conn->query("SELECT * FROM vacancy;");
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $vacancy = new Vacancy(
                (int) $row["id"],
                strip_tags($row["title"], $allowed_tags),
                strip_tags($row["description"], $allowed_tags),
                strip_tags($row["salary_min"] . $allowed_tags),
                strip_tags($row["salary_max"], $allowed_tags),
                strip_tags($row["company_name"], $allowed_tags)
            );
            $vacancy->text_short = strip_tags($row["description_short"], $allowed_tags);
            $vacancy->spec = strip_tags($specs[$row["spec_id"]]);
            $vacancy->spec_id = strip_tags($row["spec_id"], $allowed_tags);
            $vacancy->create_date = strip_tags($row["date_create"]);
            $vacancy->region = strip_tags($regions[$row["region_id"]]);
            $vacancy->region_id = strip_tags($row["region_id"], $allowed_tags);
            array_push($vacancies_all, $vacancy);
        }
    }

    if (!array_key_exists("page", $_GET)) {
        $_GET = array_merge($_GET, array("page" => 1));
    }
    if ($_GET["page"] < 1) {
        $_GET["page"] = 1;
    }
    if (!array_key_exists("on-page", $_GET)) {
        $_GET = array_merge($_GET, array("on-page" => 12));
    }
    $k = $_GET["on-page"];
    $max_k = ceil(count($vacancies_all) / $k);
    if ($_GET["page"] > $max_k) {
        $_GET["page"] = $max_k;
    }
    $vacancies = array_slice(
        $vacancies_all, ($_GET["page"] - 1) * $k,
        $k
    );

    response(200, array("Message" => "Успех!", "Vacancies" => $vacancies, "Page" => $_GET["page"], "Max_pages" => $max_k));
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    // add new vacancy

    $c = admin_check(1, $conn);
    if ($c == 401)
        response(401, array("Message" => "Неавторизован!"));
    if ($c == 404)
        response(404, array("Message" => "Неавторизован!"));
    if ($c == 403)
        response(403, array("Message" => "Нет доступа к этой функции!"));

    $vacancy_params = array("title", "description", "description_short", "region_id", "spec_id", "salary_min", "salary_max");

    for ($i = 0; $i < count($vacancy_params); $i++) {
        if (!isset($http_body[$vacancy_params[$i]]) or trim($http_body[$vacancy_params[$i]]) == '') {
            response(
                400,
                array(
                    "Message" => "Не все поля заполнены! '$vacancy_params[$i]' неопределено!",
                    "Error" => "Ошибка параметров! '$vacancy_params[$i]' неопределено!"
                )
            );
        }
    }

    $http_title = $http_body["title"];
    $http_desc = $http_body["description"];
    $http_desc_short = $http_body["description_short"];
    $http_spec = $http_body["spec_id"];
    $http_s_min = $http_body["salary_min"];
    $http_s_max = $http_body["salary_max"];
    $http_reg = $http_body["region_id"];
    $http_company = $http_body["company_name"];
    $http_date = date('Y/m/d H:i:s a', time());

    $sql = "INSERT INTO vacancy(title, description, description_short, 
                                spec_id, salary_min, salary_max, region_id, 
                                company_name, date_create) 
            VALUES('$http_title', '$http_desc', '$http_desc_short',
                    '$http_spec', '$http_s_min', '$http_s_max', '$http_reg',
                    '$http_company', '$http_date');";
    $result = $conn->query($sql);

    if ($result == 1) {
        response(200, array("Message" => "Вакансия успешно добавлена!"));
    } else {
        response(500, array("Message" => "Ошибка БД!"));
    }
}

if ($_SERVER['REQUEST_METHOD'] == 'DELETE') {
    $c = admin_check(1, $conn);
    if ($c == 401)
        response(401, array("Message" => "Неавторизован!"));
    if ($c == 404)
        response(404, array("Message" => "Неавторизован!"));
    if ($c == 403)
        response(403, array("Message" => "Нет доступа к этой функции!"));

    if (!isset($http_body['id']) or trim($http_body['id']) == '') {
        response(400, array("Message" => "Ошибка параметров! Id неопределено!"));
    }

    $sql = "DELETE FROM vacancy WHERE id LIKE $http_body[id];";

    $result = $conn->query($sql);
    if ($result != 1) {
        response(500, array("Message" => "Ошибка БД!"));
    }

    response(200, array("Message" => "Удаление успешно!", "sadfsfsdsdf" => $sql));
}

if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
    $c = admin_check(1, $conn);
    if ($c == 401)
        response(401, array("Message" => "Неавторизован!"));
    if ($c == 404)
        response(404, array("Message" => "Неавторизован!"));
    if ($c == 403)
        response(403, array("Message" => "Нет доступа к этой функции!"));

    if (!isset($http_body['id']) or trim($http_body['id']) == '') {
        response(400, array("Message" => "Ошибка параметров! Id неопределено!"));
    }

    $vacancy_params = array("title", "description", "description_short", "region_id", "spec_id", "salary_min", "salary_max");
    for ($i = 0; $i < count($vacancy_params); $i++) {
        if (!isset($http_body[$vacancy_params[$i]]) or trim($http_body[$vacancy_params[$i]]) == '') {
            response(
                400,
                array(
                    "Message" => "Не все поля заполнены! '$vacancy_params[$i]' неопределено!",
                    "Error" => "Ошибка параметров! '$vacancy_params[$i]' неопределено!"
                )
            );
        }
    }

    $sql = "UPDATE vacancy SET title='$http_body[title]',";
    $sql .= "description='$http_body[description]',";
    $sql .= "description_short='$http_body[description_short]',";
    $sql .= "spec_id='$http_body[spec_id]',";
    $sql .= "region_id ='$http_body[region_id]',";
    $sql .= "salary_min='$http_body[salary_min]',";
    $sql .= "salary_max='$http_body[salary_max]',";
    $sql .= "company_name='$http_body[company_name]'";
    $sql .= "WHERE id LIKE $http_body[id];";

    $result = $conn->query($sql);
    if ($result != 1) {
        response(500, array("Message" => "Ошибка БД!"));
    }

    response(200, array("Message" => "Обновление успешно!"));
}


?>