<?php
function response($code, $response_data = NULL)
{
    ob_clean();
    header("HTTP/1.1 $code");
    header('Content-Type: application/json; charset=utf-8');

    if ($response_data != NULL) {
        echo json_encode($response_data, JSON_UNESCAPED_UNICODE);
    }

    exit();
}
function admin_check($level, $conn)
{
    $token = UserService::getJWTToken();
    if (!$token) {
        return 401;
    } else {
        $user = UserService::decodeToken($token, $conn);
        if (!$user)
            return 404;
        if ($user->role < $level)
            return 403;
    }
    return 200;
}
?>