<?php
// get my profile
if ($_SERVER['REQUEST_METHOD'] == "GET") {
    $token = UserService::getJWTToken();
    if ($token) {
        $user = UserService::decodeToken($token, $conn);
        if (!$user) {
            response(404, array("Message" => "Неправильный токен!"));
        }
        response(200, array(
            "Message" => "Профиль получен!",
            "Email" => $user->email,
            "Nickname" => $user->nickname,
            "Description" => $user->description,
            "Role" => $user->role,
        )
        );
    }
    response(401, array("Message" => "Неавторизован!"));
}

// edit my profile
if ($_SERVER['REQUEST_METHOD'] == "POST") {
    $http_body = json_decode(file_get_contents('php://input'), true);
    $token = UserService::getJWTToken();
    if ($token) {
        $user = new User();
        $user->email = filter_var($http_body["Email"], FILTER_SANITIZE_EMAIL);
        $user->nickname = filter_var($http_body["Nickname"], FILTER_UNSAFE_RAW);
        $user->description = filter_var($http_body["Description"], FILTER_UNSAFE_RAW);
        if (!UserService::checkToken($user, $token)) {
            response(403, array("Message" => "Токен не совпадает!"));
        }
        $sql = "UPDATE user_info SET nickname='$user->nickname', description='$user->description'";
        $sql .= " WHERE email='$user->email';";
        $result = $conn->query($sql);
        response(200, array("Message" => "Профиль сохранен!"));
    }
    response(401, array("Message" => "Неавторизован!"));
}
?>