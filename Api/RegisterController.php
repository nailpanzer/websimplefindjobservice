<?php
$http_body = json_decode(file_get_contents('php://input'), true);
if (
    !isset($http_body["email"])
    or !isset($http_body["nickname"])
    or !isset($http_body["password"])
) {
    response(400, array("Message" => "Ошибка параметров! Принимает email, nickname, password!"));
}
if ($http_body["email"] == "" or $http_body["nickname"] == "" or $http_body["password"] == "") {
    response(400, array("Message" => "Введены пустые строки!"));
}

// filters
$http_email = $http_body["email"];
if (!filter_var($http_email, FILTER_VALIDATE_EMAIL)) {
    response(400, array("Message" => "Некорректный email '$http_email'"));
}

$sql = "SELECT * FROM user_info WHERE email LIKE '$http_email';";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    response(400, array("Message" => "Email уже зарегестрирован!"));
}

$http_password = $http_body["password"];
$http_nickname = $http_body["nickname"];
$sql = "INSERT INTO user_info(nickname, password, email) VALUES('$http_nickname', '$http_password', '$http_email');";
$result = $conn->query($sql);

if ($result == 1) {
    response(200, array("Message" => "Регистрация проведена успешно!"));
} else {
    response(500, array("Message" => "Ошибка БД!"));
}
exit();
?>