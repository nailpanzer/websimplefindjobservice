<?php
$http_body = json_decode(file_get_contents('php://input'), true);

if ($_SERVER['REQUEST_METHOD'] == 'GET') {

    // returns one spec
    if (array_key_exists("id", $_GET)) {
        $spec = new Spec();
        $spec->id = $_GET["id"];
        $result = $conn->query("SELECT * FROM specialization WHERE id LIKE $spec->id;");
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $spec->id = strip_tags($row["id"], $allowed_tags);
                $spec->name = strip_tags($row["name"], $allowed_tags);
                $spec->desc = strip_tags($row["description"], $allowed_tags);
            }
        } else {
            response(404, array("Message" => "Не найдено!", "Spec" => $spec));
        }

        response(200, array("Message" => "Успех!", "Spec" => $spec));
    }

    // returns all specializations
    $specs_all = array();
    $result = $conn->query("SELECT * FROM specialization;");
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $spec = new Spec();
            $spec->id = strip_tags($row["id"], $allowed_tags);
            $spec->name = strip_tags($row["name"], $allowed_tags);
            $spec->desc = strip_tags($row["description"], $allowed_tags);
            array_push($specs_all, $spec);
        }
    }

    if (!array_key_exists("page", $_GET)) {
        $_GET = array_merge($_GET, array("page" => 1));
    }
    if ($_GET["page"] < 1) {
        $_GET["page"] = 1;
    }
    if (!array_key_exists("on-page", $_GET)) {
        $_GET = array_merge($_GET, array("on-page" => 12));
    }
    $k = $_GET["on-page"];
    $max_k = ceil(count($specs_all) / $k);
    if ($_GET["page"] > $max_k) {
        $_GET["page"] = $max_k;
    }
    $specs = array_slice(
        $specs_all, ($_GET["page"] - 1) * $k,
        $k
    );

    response(200, array("Message" => "Успех!", "Specs" => $specs, "Page" => $_GET["page"], "Max_pages" => $max_k));
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    // add new specializations

    $c = admin_check(1, $conn);
    if ($c == 401)
        response(401, array("Message" => "Неавторизован!"));
    if ($c == 404)
        response(404, array("Message" => "Неавторизован!"));
    if ($c == 403)
        response(403, array("Message" => "Нет доступа к этой функции!"));

    $requared_params = array("name");
    for ($i = 0; $i < count($requared_params); $i++) {
        if (!isset($http_body[$requared_params[$i]]) or trim($http_body[$requared_params[$i]]) == '') {
            response(
                400,
                array(
                    "Message" => "Не все поля заполнены! '$requared_params[$i]' неопределено!",
                    "Error" => "Ошибка параметров! '$requared_params[$i]' неопределено!"
                )
            );
        }
    }

    $http_name = $http_body["name"];
    $http_desc = "";
    if (array_key_exists("description", $_GET)) {
        $http_desc = $http_body["description"];
    }

    $result = $conn->query("INSERT INTO specialization(name, description)
        VALUES('$http_name', '$http_desc');");

    if ($result == 1) {
        response(200, array("Message" => "Специализация успешно добавлена!"));
    } else {
        response(500, array("Message" => "Ошибка БД!"));
    }
}

if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
    // update specialization

    $c = admin_check(1, $conn);
    if ($c == 401)
        response(401, array("Message" => "Неавторизован!"));
    if ($c == 404)
        response(404, array("Message" => "Неавторизован!"));
    if ($c == 403)
        response(403, array("Message" => "Нет доступа к этой функции!"));

    if (!isset($http_body['id']) or trim($http_body['id']) == '') {
        response(400, array("Message" => "Ошибка параметров! Id неопределено!"));
    }

    $requared_params = array("name");
    for ($i = 0; $i < count($requared_params); $i++) {
        if (!isset($http_body[$requared_params[$i]]) or trim($http_body[$requared_params[$i]]) == '') {
            response(
                400,
                array(
                    "Message" => "Не все поля заполнены! '$requared_params[$i]' неопределено!",
                    "Error" => "Ошибка параметров! '$requared_params[$i]' неопределено!"
                )
            );
        }
    }

    $sql = "UPDATE specialization SET name='$http_body[name]',";
    $sql .= "name='$http_body[name]',";
    $sql .= "description='$http_body[description]'";
    $sql .= "WHERE id LIKE $http_body[id];";

    $result = $conn->query($sql);
    if ($result != 1) {
        response(500, array("Message" => "Ошибка БД!"));
    }

    response(200, array("Message" => "Обновление успешно!"));
}


?>