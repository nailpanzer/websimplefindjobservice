<?php
$http_body = json_decode(file_get_contents('php://input'), true);
$token = UserService::getJWTToken();
if ($token) {
    $user = UserService::decodeToken($token, $conn);
    if (!$user) {
        response(404, array("Message" => "Неправильный токен!"));
    }
} else {
    response(401, array("Message" => "Неавторизован!"));
}

// add new favourite
if ($_SERVER['REQUEST_METHOD'] == "POST") {
    $vacancy_id = (int) $http_body["vacancy_id"];
    $result = $conn->query("SELECT * FROM favourites WHERE user_id='$user->id' AND vacancy_id='$vacancy_id';");
    if ($result->num_rows > 0) {
        response(400, array("Message" => "Уже в избранном!"));
    }
    $result = $conn->query("INSERT INTO favourites(user_id, vacancy_id) VALUES('$user->id', '$vacancy_id');");
    response(200, array("Message" => "Добавлено в избранное!"));
}

// delete favourite
if ($_SERVER['REQUEST_METHOD'] == "DELETE") {
    $vacancy_id = (int) $http_body["vacancy_id"];
    $result = $conn->query("SELECT * FROM favourites WHERE user_id='$user->id' AND vacancy_id='$vacancy_id';");
    if ($result->num_rows == 1) {
        $conn->query("DELETE FROM favourites WHERE user_id='$user->id' AND vacancy_id='$vacancy_id';");
        response(200, array("Message" => "Удалено из избранного!"));
    }
    response(400, array("Message" => "Нет в избранном!"));
}

// get my favourites
if ($_SERVER['REQUEST_METHOD'] == "GET") {
    $result = $conn->query("SELECT * FROM favourites WHERE user_id='$user->id';");
    $favourites = array();
    $favourites_titles = array();
    while ($row = $result->fetch_assoc()) {
        $vacancy_id = (int) $row["vacancy_id"];
        $result_title = $conn->query("SELECT * FROM vacancy WHERE id='$vacancy_id';");
        $row_title = $result_title->fetch_assoc();
        array_push($favourites, $vacancy_id);
        array_push($favourites_titles, filter_var($row_title["title"], FILTER_UNSAFE_RAW));
    }
    response(200, array(
        "Message" => "Избранное получено!",
        "Favourites" => $favourites,
        "FavouritesTitles" => $favourites_titles,
    )
    );
    exit();
}
?>