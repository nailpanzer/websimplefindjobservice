<?php
$http_body = json_decode(file_get_contents('php://input'), true);
if (
    !isset($http_body["email"])
    or !isset($http_body["password"])
) {
    response(400, array("Message" => "Ошибка параметров! Принимает email, password!"));
}
if ($http_body["email"] == "" or $http_body["password"] == "") {
    response(400, array("Message" => "Введены пустые строки!"));
}
$http_email = $http_body["email"];
if (!filter_var($http_email, FILTER_VALIDATE_EMAIL)) {
    response(400, array("Message" => "Некорректный email!"));
}

$sql = "SELECT * FROM user_info WHERE email LIKE '$http_email';";
$result = $conn->query($sql);
if ($result->num_rows == 0) {
    response(400, array("Message" => "Email не найден!"));
}
if ($result->num_rows > 1) {
    response(400, array(
        "Message" => "Ошибка БД. Множество найденых Email! Проверьте корректность введенных данных!"
    )
    );
}
$http_password = $conn->real_escape_string($http_body["password"]);
if ($result->num_rows == 1) {
    while ($row = $result->fetch_assoc()) {
        if ($row["password"] == $http_password) {
            // jwt authentication
            $user = new User();
            $user->email = $http_email;
            $JWT_token = UserService::encodeToken($user);
            response(200, array(
                "Message" => "Авторизация выполнена успешно!",
                "Token" => $JWT_token
            )
            );
        } else {
            response(400, array("Message" => "Неверный пароль!"));
        }
    }
}

exit();
?>